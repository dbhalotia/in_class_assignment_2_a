@extends('sonia.sonia-layout')

@section('content')

      <!-- Blog Entries Column -->
      <div class="col-md-8">

        <h1 class="my-4">Page Heading
          <small>Secondary Text</small>
        </h1>

        

        <!-- Blog Post -->
        <div class="card mb-4">
          
          <div class="card-body">
            <h2 class="card-title">Dog List</h2>
            @foreach($dogs as $dog)
            <p class="card-text">{{$dog->name}} <a href="/dogs/{{$dog->name}}" class="btn btn-primary">Details </a></p>
             @endforeach
          </div>
          
        </div>

      </div>

     

@endsection