@extends('cars.cars_layout')

@section('content')

<!-- Blog Entries Column -->
      <div class="col-md-8">

        <h1 class="my-4">Page Heading
          <small>Secondary Text</small>
        </h1>

        <!-- Blog Post -->
        <div class="card mb-4">
         
          <div class="card-body">
            <h2 class="card-title">CAR LIST</h2>
            @foreach($cars as $car)
            <p class="card-text"><a href="/cars/{{$car->id}}">{{$car->name}}</a></p>
            @endforeach
            
            <div class="card-footer text-muted">
            Posted on {{Carbon\Carbon::parse($car->created_at)->diffForHumans()}}
          </div>
          </div>
          
        </div>

        

        <!-- Pagination -->
        <ul class="pagination justify-content-center mb-4">
          <li class="page-item">
            <a class="page-link" href="#">&larr; Older</a>
          </li>
          <li class="page-item disabled">
            <a class="page-link" href="#">Newer &rarr;</a>
          </li>
        </ul>

      </div>

@endsection